package ConfigurationManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Configuration implements Serializable {
    private HashMap<String,Object> parametres;
    public Configuration(){
        this.parametres=new HashMap<String,Object>();
    }
    public Configuration(Map<String,Object> parametrs){
        if (parametrs!=null){
            this.parametres=new HashMap<String,Object>(parametrs);
        }else throw new NullPointerException();
    }
    public boolean AddParametr(String name,Object value) {
        if ( parametres.put(name, value)!=null) {
            return true;
        }else {
           return false;
        }
    }
    public Object GetParametr(String name)throws NullPointerException{
        return parametres.get(name);
    }

    @Override
    public String toString() {
        String out=new String(super.toString()+"\n");
        for (Map.Entry<String,Object> param:parametres.entrySet()) {
            out+=param.toString()+"\n";
        }
        return out;
    }
}
