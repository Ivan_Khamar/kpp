package ConfigurationManager;

import java.io.*;

public class ConfigurationManager {
    static{
        configurationManager = new ConfigurationManager();
    }

    private Configuration configuration;
    private  static final String  CONFIG_DIRECTORY= "Configurations";
    private  static final String FILE_EXTENSION = ".config";
    private static String fileName=new String(CONFIG_DIRECTORY+"/"+"config"+FILE_EXTENSION);
    ConfigurationManager(){
        configuration=new Configuration();
        new File(CONFIG_DIRECTORY).mkdir();
    }
    boolean StoreConfiguration() throws IOException {
        FileOutputStream out=new FileOutputStream(fileName,false);
        ObjectOutputStream objectOut=new ObjectOutputStream(out);
        objectOut.writeObject(configuration);
        objectOut.close();
        out.close();
        return true;
    }
    boolean LoadConfiguration() throws IOException, ClassNotFoundException {
        FileInputStream input=new FileInputStream(fileName);
        ObjectInputStream objectInput=new ObjectInputStream(input);
        configuration=(Configuration) objectInput.readObject();
        return true;
    }
//    public void SetConfiguration(Configuration configuration)throws NullPointerException{
//        if (configuration!=null) {
//            this.configuration = configuration;
//        }else {
//            throw new NullPointerException();
//        }
//    }

    private static boolean isInited;
    private static ConfigurationManager configurationManager;

    public static Configuration getConfiguration() throws IOException, ClassNotFoundException {
        if (!isInited) {
            configurationManager.LoadConfiguration();
        }

        return configurationManager.configuration;
    }

    public static void Save() throws IOException {
        configurationManager.StoreConfiguration();
    }
}
