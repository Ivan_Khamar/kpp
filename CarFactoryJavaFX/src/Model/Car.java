package Model;
import java.util.Objects;

public class Car {
    private int carId;
    private CarPart body;
    private CarPart engine;
    private CarPart accessories;
    public Car(int carId,CarPart body,CarPart engine,CarPart accessories) {
        this.carId=carId;
        this.body=body;
        this.engine=engine;
        this.accessories=accessories;
    }
    public int getCarId() {
        return carId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return carId == car.carId;
    }
    @Override
    public int hashCode() {
        return Objects.hash(carId);
    }
    @Override
    public String toString() {
        return "Car with id: "+carId;
    }
}
