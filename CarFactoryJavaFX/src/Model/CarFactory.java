package Model;
import ThreadPool.ThreadPool;

public class CarFactory {
    private PartStorage bodiesStorage;
    private PartStorage enginesStorage;
    private PartStorage acessoriesStorage;
    private CarStorage carStorage;
    private ThreadPool workersPool;
    private Worker worker;
    private volatile static Integer CarId = new Integer(1);
    public CarFactory(){
    workersPool=new ThreadPool(10);
    }
    public CarFactory(int workerCount ,int workTime,PartStorage bodiesStorage,PartStorage enginesStorage, PartStorage acessoriesStorage, CarStorage carStorage){
        workersPool=new ThreadPool(workerCount);
        this.worker=new Worker(this,workTime);
        this.bodiesStorage=bodiesStorage;
        this.enginesStorage=enginesStorage;
        this.acessoriesStorage=acessoriesStorage;
        this.carStorage=carStorage;
    }
    public void ChangeTimeInterval(int workTime){
        worker.setWorkTime(workTime);
    }
    public int GetCarId(){
        synchronized (CarId){
            return  ++CarId;
        }
    }
    public int GetCurrentTaskCount(){
       return workersPool.getQueueSize();
    }
    public CarStorage getCarStorage() {
        return carStorage;
    }
    public PartStorage getBodiesStorage() {
        return bodiesStorage;
    }
    public PartStorage getAcessoriesStorage() {
        return acessoriesStorage;
    }
    public PartStorage getEnginesStorage() {
        return enginesStorage;
    }
    public void pushCarBuildTask(int carCount){
        for (int i = 0; i < carCount; i++) {
            workersPool.execute(worker);
        }
    }
}
