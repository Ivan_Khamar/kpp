package Model;

import java.util.Objects;

public  class CarPart {
    private int PartId;
    private PartType type;
    public CarPart(int id,PartType type){
        this.PartId=id;
        this.type=type;
    }
    public int getPartId() {
        return PartId;
    }
    public PartType getType() {
        return type;
    }
    @Override
    public String toString() {
        String out=new String();
        switch (type) {
            case BODY -> {
                out += new String("Body");
                break;
            }
            case ENGINE -> {
                out += new String("Engine");
                break;
            }
            case ACCESSORIES -> {
                out += new String("Accessories");
                break;
            }
            default -> {
                out += new String("Error");
                break;
            }
        }
        out+=new String("with index : "+PartId);
        return out;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarPart carPart = (CarPart) o;
        return PartId == carPart.PartId && type == carPart.type;
    }
    @Override
    public int hashCode() {
        return Objects.hash(PartId, type);
    }
}
