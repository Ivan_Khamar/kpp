package Model;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CarStorage{
    private LinkedList<Car> storedCar;
    private int maxSize;
    private volatile int vCarCount;
    private ExecutorService controllerExecutor;
    private Runnable storageController;
    public CarStorage(int maxSize){
        this.maxSize=maxSize;
        this.storedCar=new LinkedList<Car>();
        this.vCarCount=0;
        this.controllerExecutor= Executors.newSingleThreadExecutor();
    }
    public void setStorageController(Runnable storageController) {
        this.storageController = storageController;
    }

    public boolean AddCar(Car car) throws InterruptedException {
        synchronized (storedCar) {
            if (vCarCount < maxSize) {
                ++vCarCount;
                storedCar.add(car);
                storedCar.notify();
                return true;
            } else {
                storedCar.wait();
                return false;
            }
        }
    }
    public Car GetCar() throws InterruptedException {
        synchronized (storedCar) {
            if(vCarCount<maxSize){
                controllerExecutor.execute(storageController);
            }
            if (vCarCount > 0) {
                --vCarCount;
                Car temp = storedCar.get(0);
                storedCar.notify();
                return temp;
            } else {
                storedCar.wait();
                return null;
            }

        }
    }
    public int getCarCount() {
        return vCarCount;
    }
    public int getMaxSize() {
        return maxSize;
    }
}