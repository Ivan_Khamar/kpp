package Model;

public class CarStorageController implements Runnable {
    private CarStorage storage;
    private CarFactory factory;
    public CarStorageController( CarFactory factory){
        this.storage=factory.getCarStorage();
        this.factory=factory;
    }
    @Override
    public void run() {
        int carCount=storage.getMaxSize()- storage.getCarCount()-factory.GetCurrentTaskCount();
        if (carCount>0){
            System.out.println("Pushed task to build cars");
            factory.pushCarBuildTask(carCount);
        }

    }
}
