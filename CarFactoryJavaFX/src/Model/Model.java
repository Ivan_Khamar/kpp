package Model;

import ConfigurationManager.Configuration;
import Views.ConfigurationSettings;

import java.util.ArrayList;
import java.util.List;

import static Views.ConfigurationSettings.*;

public class Model {
    public List<Supplier> bodySuppliers = new ArrayList<Supplier>();
    public List<Supplier> engineSuppliers = new ArrayList<Supplier>();
    public List<Supplier> accessoriesSuppliers = new ArrayList<Supplier>();

    public PartStorage bodyStorage;
    public PartStorage engineStorage;
    public PartStorage accessoryStorage;

    public CarFactory carFactory;

    public CarStorage carStorage;
    public CarStorageController carStorageController;

    public List<Dealer> dealers = new ArrayList<Dealer>();

    public  Model() {
        Configuration conf = ConfigurationSettings.configuration;

        bodyStorage = new PartStorage((int)conf.GetParametr(STORAGE_BODY_CAPACITY_CONF), PartType.BODY);
        engineStorage = new PartStorage((int)conf.GetParametr(STORAGE_ENGINE_CAPACITY_CONF), PartType.ENGINE);
        accessoryStorage = new PartStorage((int)conf.GetParametr(STORAGE_ACCESSORIES_CAPACITY_CONF), PartType.ACCESSORIES);

        for (int i = 0; i < (int)conf.GetParametr(SUPPLIER_BODY_AMOUNT_CONF); ++i) {
            Supplier supplier = new Supplier((int)conf.GetParametr(SUPPLIER_BODY_INTERVAL_CONF),
                    PartType.BODY,
                    bodyStorage);
            bodySuppliers.add(supplier);
            System.out.println(supplier.Start());
        }

        for (int i = 0; i < (int)conf.GetParametr(SUPPLIER_ENGINE_AMOUNT_CONF); ++i) {
            Supplier supplier = new Supplier((int)conf.GetParametr(SUPPLIER_ENGINE_INTERVAL_CONF),
                    PartType.ENGINE,
                    engineStorage);
            engineSuppliers.add(supplier);
            System.out.println(supplier.Start());
        }

        for (int i = 0; i < (int)conf.GetParametr(SUPPLIER_ACCESSORIES_AMOUNT_CONF); ++i) {
            Supplier supplier = new Supplier((int)conf.GetParametr(SUPPLIER_ACCESSORIES_INTERVAL_CONF),
                    PartType.ACCESSORIES,
                    accessoryStorage);
            accessoriesSuppliers.add(supplier);
            System.out.println(supplier.Start());
        }

        carStorage = new CarStorage((int)conf.GetParametr(STORAGE_CAR_CAPACITY_CONF));

        carFactory = new CarFactory((int)conf.GetParametr(WORKER_AMOUNT_CONF),
                (int)conf.GetParametr(CAR_FACTORY_INTERVAL_CONF),
                bodyStorage,
                engineStorage,
                accessoryStorage,
                carStorage);

        carStorageController = new CarStorageController(carFactory);
        carStorage.setStorageController(carStorageController);

        carStorageController.run();

        for (int i = 0; i < (int)conf.GetParametr(DILLER_AMOUNT_CONF); ++i) {
            Dealer dealer = new Dealer((int)conf.GetParametr(DILLER_INTERVAL_CONF), carStorage);
            dealers.add(dealer);
            System.out.println(dealer.Start());
        }
    }
}
