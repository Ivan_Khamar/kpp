package Model;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ModelTimerThread implements Runnable {
    protected Thread thread;
    private int timeInterval;
    private ScheduledFuture<?> future;
    private ScheduledExecutorService executorService;
    private volatile Boolean isRunning=Boolean.FALSE;
    protected Runnable task;
    private volatile Boolean isExited=Boolean.FALSE;
    public ModelTimerThread(int timeInterval){
        this.timeInterval=timeInterval;
        this.task=null;
        this.executorService= Executors.newScheduledThreadPool(1);
        this.thread=new Thread(this);
    }
    public void ChangeTimeInterval(int interval){
        if ((interval>0)&&(this.timeInterval!=interval)){
            this.timeInterval=interval;
            if (isRunning){
                if ((!future.isCancelled()&&(future!=null))){
                    future.cancel(true);
                }
                future=executorService.scheduleAtFixedRate(task,interval,interval, TimeUnit.MILLISECONDS);
            }
        }
    }
    public int GetTimeInterval(){
        return timeInterval;
    }
    public int Exit(){
        isExited=Boolean.TRUE;
        future.cancel(true);
        return 0;
    }
    public boolean Stop(){
        synchronized (isRunning) {
            if (isRunning == true) {
                isRunning = false;
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean Start(){
        if((!thread.isAlive())&&(!thread.isInterrupted())){
            thread.start();
        }
        synchronized (isRunning) {
            if (isRunning == false) {
                isRunning = true;
                return true;
            } else {
                return false;
            }
        }
    }
    @Override
    public void run() {
        try {
            while (!isExited) {
                if ((isRunning) &&(future==null)|| (future.isCancelled())) {
                    future = executorService.scheduleAtFixedRate(task, timeInterval, timeInterval, TimeUnit.MILLISECONDS);
                    thread.sleep(timeInterval);
                }
            }
        }catch (InterruptedException exception){
            System.out.println(exception.getMessage());
        }
    }
}
