package Model;
import java.util.LinkedList;

public class PartStorage {
    private final LinkedList<CarPart> storedPart;
    private final PartType storedPartsType;
    private final int maxSize;
    private volatile int vStoredPartCount;
    public PartStorage(int maxSize,PartType vStoredPartsType) {
        this.storedPart = new LinkedList<CarPart>();
        this.maxSize = maxSize;
        this.storedPartsType=vStoredPartsType;
        vStoredPartCount = 0;
    }
    public boolean AddPart(CarPart part)throws InterruptedException {
        synchronized (storedPart) {
            if (vStoredPartCount < maxSize) {
                ++vStoredPartCount;
                storedPart.add(part);
                storedPart.notify();
                return true;
            } else {
                storedPart.wait();
                return false;
            }
        }
    }

    public  PartType getStoredPartsType(){
        return storedPartsType;
    }
    public int getStoredPartCount() {
        return vStoredPartCount;
    }

    public CarPart GetCarPart() throws InterruptedException {
        synchronized (storedPart) {
            if (vStoredPartCount > 0) {
                --vStoredPartCount;
                CarPart temp = storedPart.get(0);
                storedPart.notify();
                return temp;
            } else {
                storedPart.wait();
                return null;
            }
        }
    }
}