package Model;

public class Supplier extends ModelTimerThread{
    private PartType suppliePartType;
    private volatile static Integer idCounter=0;
    private PartStorage targetStorage;
    public Supplier(int interval, PartType suppliePartType, PartStorage targetStorage){
        super(interval);
        this.targetStorage=targetStorage;
        task=()->{
            try {
                CarPart part;
                synchronized (idCounter) {
                    part = new CarPart(++idCounter, suppliePartType);
                }
                    //Tests
                System.out.println(part + " "+" by thread"+" "+ this.thread.getName());
                targetStorage.AddPart(part);
            }catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        };

    }
    public PartStorage getTargetStorage() {
        return targetStorage;
    }
    public PartType getSuppliePartType() {
        return suppliePartType;
    }
}
