package Model;

public class Worker implements Runnable {
    private CarFactory factory;
    private int workTime;
    public Worker(CarFactory factory, int workTime){
        this.factory=factory;
        this.workTime=workTime;
    }
    public void setWorkTime(int workTime){
        if(workTime>0){
        this.workTime=workTime;
        }
    }
    public int getWorkTime() {
        return workTime;
    }
    @Override
    public void run() {
        try{
    CarPart body=factory.getBodiesStorage().GetCarPart();
    CarPart engine=factory.getEnginesStorage().GetCarPart();
    CarPart accessories=factory.getAcessoriesStorage().GetCarPart();
    Thread.currentThread().sleep(workTime);

    System.out.println("Car build start");

    factory.getCarStorage().AddCar(new Car(factory.GetCarId(), body,engine,accessories));
        }catch (InterruptedException exception){
            System.out.println(exception.getMessage());
        }
    }
}
