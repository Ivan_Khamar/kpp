package ThreadPool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPool implements Executor {
    private final BlockingQueue<Runnable> workQueue;
    private volatile boolean isRunning=true;
    private volatile int queueSize;
    public ThreadPool(int workerCount){
        workQueue=new LinkedBlockingQueue<Runnable>(workerCount);
        queueSize=0;
        for (int i = 0; i < workerCount; i++) {
            new Thread(new TaskWorker()).start();
        }
    }
    @Override
    public void execute(Runnable command) {
        if (isRunning) {
            workQueue.offer(command);
            ++queueSize;
        }
    }
    public int getQueueSize(){
        return queueSize;
    }
    public void shutdown() {
        isRunning = false;
    }
    private final class TaskWorker implements Runnable {
        @Override
        public void run() {
            while (isRunning) {
                Runnable nextTask = workQueue.poll();
                --queueSize;
                if (nextTask != null) {
                    nextTask.run();
                }
            }
        }
    }
}
