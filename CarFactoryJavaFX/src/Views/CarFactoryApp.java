package Views;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class CarFactoryApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent newRoot = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));// .load(getClass().getResource("MainWindow.fxml"));
        primaryStage.setTitle("Car Factory");
        primaryStage.setScene(new Scene(newRoot, 500, 500));
        primaryStage.show();
    }
}
