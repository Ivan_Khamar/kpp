package Views;

import ConfigurationManager.Configuration;
import ConfigurationManager.ConfigurationManager;
import Model.Model;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class ConfigurationSettings extends JFrame {

    public static Model model;

    public final static String SUPPLIER_BODY_AMOUNT_CONF = "supplier_body_amount_conf";
    public final static String SUPPLIER_ENGINE_AMOUNT_CONF = "supplier_engine_amount_conf";
    public final static String SUPPLIER_ACCESSORIES_AMOUNT_CONF = "supplier_accessories_amount_conf";
    public final static String SUPPLIER_BODY_INTERVAL_CONF = "supplier_body_interval_conf";
    public final static String SUPPLIER_ENGINE_INTERVAL_CONF = "supplier_engine_interval_conf";
    public final static String SUPPLIER_ACCESSORIES_INTERVAL_CONF = "supplier_accessories_interval_conf";
    public final static String CAR_FACTORY_INTERVAL_CONF = "car_factory_interval_conf";
    public final static String DILLER_INTERVAL_CONF = "diller_interval_conf";
    public final static String DILLER_AMOUNT_CONF = "diller_amount_conf";
    public final static String WORKER_AMOUNT_CONF = "worcker_amount_conf";
    public final static String STORAGE_CAR_CAPACITY_CONF = "storage_car_capacity_conf";
    public final static String STORAGE_BODY_CAPACITY_CONF = "storage_body_capacity_conf";
    public final static String STORAGE_ENGINE_CAPACITY_CONF = "storage_engine_capacity_conf";
    public final static String STORAGE_ACCESSORIES_CAPACITY_CONF = "storage_accessories_capacity_conf";

    public final static int SUPPLIER_BODY_AMOUNT_MAX = 5;
    public final static int SUPPLIER_ENGINE_AMOUNT_MAX = 5;
    public final static int SUPPLIER_ACCESSORIES_AMOUNT_MAX = 5;
    public final static int SUPPLIER_BODY_INTERVAL_MAX = 50;
    public final static int SUPPLIER_ENGINE_INTERVAL_MAX = 50;
    public final static int SUPPLIER_ACCESSORIES_INTERVAL_MAX = 50;
    public final static int CAR_FACTORY_INTERVAL_MAX = 50;
    public final static int DILLER_INTERVAL_MAX = 50;
    public final static int DILLER_AMOUNT_MAX = 5;
    public final static int WORKER_AMOUNT_MAX = 5;
    public final static int STORAGE_CAR_CAPACITY_MAX = 20;
    public final static int STORAGE_BODY_CAPACITY_MAX = 20;
    public final static int STORAGE_ENGINE_CAPACITY_MAX = 20;
    public final static int STORAGE_ACCESSORIES_CAPACITY_MAX = 20;

    public ConfigurationSettings() throws IOException, ClassNotFoundException {
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                try {
                    //ConfigurationManager.SetConfiguration(configuration);
                    ConfigurationManager.Save();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        configuration = ConfigurationManager.getConfiguration();

        setContentPane(rootPanel);
        setVisible(true);

        setMinimumSize(new Dimension(400, 435));

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        if (configuration.GetParametr(SUPPLIER_BODY_AMOUNT_CONF) == null) {
            configuration.AddParametr(SUPPLIER_BODY_AMOUNT_CONF, 2);
        }

        SpinnerNumberModel snmBody = new SpinnerNumberModel();
        snmBody.setMinimum(1);
        snmBody.setMaximum(SUPPLIER_BODY_AMOUNT_MAX);
        supplierBodyAmountSpinner.setModel(snmBody);

        if (configuration.GetParametr(SUPPLIER_BODY_AMOUNT_CONF) == null) {
            configuration.AddParametr(SUPPLIER_BODY_AMOUNT_CONF, 2);
        }
        supplierBodyAmountSpinner.setValue(configuration.GetParametr(SUPPLIER_BODY_AMOUNT_CONF));

        supplierBodyAmountSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(SUPPLIER_BODY_AMOUNT_CONF, supplierBodyAmountSpinner.getValue());
                //change body amount
            }
        });

        SpinnerNumberModel snmEngine = new SpinnerNumberModel();
        snmEngine.setMinimum(1);
        snmEngine.setMaximum(SUPPLIER_ENGINE_AMOUNT_MAX);
        supplierEngineAmountSpinner.setModel(snmEngine);
        if (configuration.GetParametr(SUPPLIER_ENGINE_AMOUNT_CONF) == null) {
            configuration.AddParametr(SUPPLIER_ENGINE_AMOUNT_CONF, 2);
        }
        supplierEngineAmountSpinner.setValue(configuration.GetParametr(SUPPLIER_ENGINE_AMOUNT_CONF));

        supplierEngineAmountSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(SUPPLIER_ENGINE_AMOUNT_CONF, supplierEngineAmountSpinner.getValue());
                // change engine amount
            }
        });

        SpinnerNumberModel snmAccessories = new SpinnerNumberModel();
        snmAccessories.setMinimum(1);
        snmAccessories.setMaximum(SUPPLIER_ACCESSORIES_AMOUNT_MAX);
        supplierAccessoriesAmountSpinner.setModel(snmAccessories);
        if (configuration.GetParametr(SUPPLIER_ACCESSORIES_AMOUNT_CONF) == null) {
            configuration.AddParametr(SUPPLIER_ACCESSORIES_AMOUNT_CONF, 2);
        }
        supplierAccessoriesAmountSpinner.setValue(configuration.GetParametr(SUPPLIER_ACCESSORIES_AMOUNT_CONF));

        supplierAccessoriesAmountSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(SUPPLIER_ACCESSORIES_AMOUNT_CONF, supplierAccessoriesAmountSpinner.getValue());
                // change accssesories amount
            }
        });

        SpinnerNumberModel snmDillerAmnt = new SpinnerNumberModel();
        snmDillerAmnt.setMinimum(1);
        snmDillerAmnt.setMaximum(DILLER_AMOUNT_MAX);
        dillerAmountSpinner.setModel(snmDillerAmnt);
        if (configuration.GetParametr(DILLER_AMOUNT_CONF) == null) {
            configuration.AddParametr(DILLER_AMOUNT_CONF, 2);
        }
        dillerAmountSpinner.setValue(configuration.GetParametr(DILLER_AMOUNT_CONF));

        dillerAmountSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(DILLER_AMOUNT_CONF, dillerAmountSpinner.getValue());
                // change diller amount
            }
        });

        SpinnerNumberModel snmWorkerAmnt = new SpinnerNumberModel();
        snmWorkerAmnt.setMinimum(1);
        snmWorkerAmnt.setMaximum(WORKER_AMOUNT_MAX);
        workerAmountSpinner.setModel(snmWorkerAmnt);
        if (configuration.GetParametr(WORKER_AMOUNT_CONF) == null) {
            configuration.AddParametr(WORKER_AMOUNT_CONF, 2);
        }
        workerAmountSpinner.setValue(configuration.GetParametr(WORKER_AMOUNT_CONF));

        workerAmountSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(WORKER_AMOUNT_CONF, workerAmountSpinner.getValue());
                // change worker amount
            }
        });

        SpinnerNumberModel snmStorageBodyCapacity = new SpinnerNumberModel();
        snmStorageBodyCapacity.setMinimum(1);
        snmStorageBodyCapacity.setMaximum(STORAGE_BODY_CAPACITY_MAX);
        storageBodyCapacitySpinner.setModel(snmStorageBodyCapacity);
        if (configuration.GetParametr(STORAGE_BODY_CAPACITY_CONF) == null) {
            configuration.AddParametr(STORAGE_BODY_CAPACITY_CONF, 2);
        }
        storageBodyCapacitySpinner.setValue(configuration.GetParametr(STORAGE_BODY_CAPACITY_CONF));

        storageBodyCapacitySpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(STORAGE_BODY_CAPACITY_CONF, storageBodyCapacitySpinner.getValue());
                // change storage body capacity
            }
        });

        SpinnerNumberModel snmStorageCarCapacity = new SpinnerNumberModel();
        snmStorageCarCapacity.setMinimum(1);
        snmStorageCarCapacity.setMaximum(STORAGE_CAR_CAPACITY_MAX);
        storageCarCapacitySpinner.setModel(snmStorageCarCapacity);
        if (configuration.GetParametr(STORAGE_CAR_CAPACITY_CONF) == null) {
            configuration.AddParametr(STORAGE_CAR_CAPACITY_CONF, 2);
        }
        storageCarCapacitySpinner.setValue(configuration.GetParametr(STORAGE_CAR_CAPACITY_CONF));

        storageCarCapacitySpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(STORAGE_CAR_CAPACITY_CONF, storageCarCapacitySpinner.getValue());
                // change storage body capacity
            }
        });

        SpinnerNumberModel snmStorageEngineCapacity = new SpinnerNumberModel();
        snmStorageEngineCapacity.setMinimum(1);
        snmStorageEngineCapacity.setMaximum(STORAGE_ENGINE_CAPACITY_MAX);
        storageEngineCapacitySpinner.setModel(snmStorageEngineCapacity);
        if (configuration.GetParametr(STORAGE_ENGINE_CAPACITY_CONF) == null) {
            configuration.AddParametr(STORAGE_ENGINE_CAPACITY_CONF, 2);
        }
        storageEngineCapacitySpinner.setValue(configuration.GetParametr(STORAGE_ENGINE_CAPACITY_CONF));

        storageEngineCapacitySpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(STORAGE_ENGINE_CAPACITY_CONF, storageEngineCapacitySpinner.getValue());
                // change storage engine capacity
            }
        });

        SpinnerNumberModel snmStorageAccessoriesCapacity = new SpinnerNumberModel();
        snmStorageAccessoriesCapacity.setMinimum(1);
        snmStorageAccessoriesCapacity.setMaximum(STORAGE_ACCESSORIES_CAPACITY_MAX);
        storageAccessoriesCapacitySpinner.setModel(snmStorageAccessoriesCapacity);
        if (configuration.GetParametr(STORAGE_ACCESSORIES_CAPACITY_CONF) == null) {
            configuration.AddParametr(STORAGE_ACCESSORIES_CAPACITY_CONF, 2);
        }
        storageAccessoriesCapacitySpinner.setValue(configuration.GetParametr(STORAGE_ACCESSORIES_CAPACITY_CONF));

        storageAccessoriesCapacitySpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(STORAGE_ACCESSORIES_CAPACITY_CONF, storageAccessoriesCapacitySpinner.getValue());
                // change storage accessories capacity
            }
        });

        carFactoryIntervalSlider.setMinimum(1);
        carFactoryIntervalSlider.setMaximum(CAR_FACTORY_INTERVAL_MAX);
        if (configuration.GetParametr(CAR_FACTORY_INTERVAL_CONF) == null) {
            configuration.AddParametr(CAR_FACTORY_INTERVAL_CONF, 1);
        }
        carFactoryIntervalSlider.setValue((int)configuration.GetParametr(CAR_FACTORY_INTERVAL_CONF));
        carFactoryIntervalSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(CAR_FACTORY_INTERVAL_CONF, carFactoryIntervalSlider.getValue());
                // change factory speed
            }
        });

        dillerIntervalSlider.setMinimum(1);
        dillerIntervalSlider.setMaximum(DILLER_INTERVAL_MAX);
        if (configuration.GetParametr(DILLER_INTERVAL_CONF) == null) {
            configuration.AddParametr(DILLER_INTERVAL_CONF, 1);
        }
        dillerIntervalSlider.setValue((int)configuration.GetParametr(DILLER_INTERVAL_CONF));
        dillerIntervalSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(DILLER_INTERVAL_CONF, dillerIntervalSlider.getValue());
                // change diller speed
            }
        });

        dillerIntervalSlider.setMinimum(1);
        dillerIntervalSlider.setMaximum(DILLER_INTERVAL_MAX);
        if (configuration.GetParametr(DILLER_INTERVAL_CONF) == null) {
            configuration.AddParametr(DILLER_INTERVAL_CONF, 1);
        }
        dillerIntervalSlider.setValue((int)configuration.GetParametr(DILLER_INTERVAL_CONF));
        dillerIntervalSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(DILLER_INTERVAL_CONF, dillerIntervalSlider.getValue());
                // change diller speed
            }
        });

        supplierBodyIntervalSlider.setMinimum(1);
        supplierBodyIntervalSlider.setMaximum(SUPPLIER_BODY_INTERVAL_MAX);
        if (configuration.GetParametr(SUPPLIER_BODY_INTERVAL_CONF) == null) {
            configuration.AddParametr(SUPPLIER_BODY_INTERVAL_CONF, 1);
        }
        supplierBodyIntervalSlider.setValue((int)configuration.GetParametr(SUPPLIER_BODY_INTERVAL_CONF));
        supplierBodyIntervalSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(SUPPLIER_BODY_INTERVAL_CONF, supplierBodyIntervalSlider.getValue());
                // change supplier body speed
            }
        });

        supplierEngineIntervalSlider.setMinimum(1);
        supplierEngineIntervalSlider.setMaximum(SUPPLIER_ENGINE_INTERVAL_MAX);
        if (configuration.GetParametr(SUPPLIER_ENGINE_INTERVAL_CONF) == null) {
            configuration.AddParametr(SUPPLIER_ENGINE_INTERVAL_CONF, 1);
        }
        supplierEngineIntervalSlider.setValue((int)configuration.GetParametr(SUPPLIER_ENGINE_INTERVAL_CONF));
        supplierEngineIntervalSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(SUPPLIER_ENGINE_INTERVAL_CONF, supplierEngineIntervalSlider.getValue());
                // change SUPPLIER ENGINE speed
            }
        });

        supplierAccessoriesIntervalSlider.setMinimum(1);
        supplierAccessoriesIntervalSlider.setMaximum(SUPPLIER_ACCESSORIES_INTERVAL_MAX);
        if (configuration.GetParametr(SUPPLIER_ACCESSORIES_INTERVAL_CONF) == null) {
            configuration.AddParametr(SUPPLIER_ACCESSORIES_INTERVAL_CONF, 1);
        }
        supplierAccessoriesIntervalSlider.setValue((int)configuration.GetParametr(SUPPLIER_ACCESSORIES_INTERVAL_CONF));
        supplierAccessoriesIntervalSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                configuration.AddParametr(SUPPLIER_ACCESSORIES_INTERVAL_CONF, supplierAccessoriesIntervalSlider.getValue());
                // change SUPPLIER ACCESSORIES speed
            }
        });
    }

    static boolean isInitialised = false;
    static public Configuration configuration;

    public JPanel rootPanel;
    private JSpinner supplierBodyAmountSpinner;
    private JSlider carFactoryIntervalSlider;
    private JSlider dillerIntervalSlider;
    private JSpinner supplierAccessoriesAmountSpinner;
    private JSpinner supplierEngineAmountSpinner;
    private JSpinner storageBodyCapacitySpinner;
    private JSpinner storageEngineCapacitySpinner;
    private JSpinner storageAccessoriesCapacitySpinner;
    private JSpinner dillerAmountSpinner;
    private JSpinner workerAmountSpinner;
    private JLabel supplierIntervalSlider;
    private JSlider supplierBodyIntervalSlider;
    private JSlider supplierEngineIntervalSlider;
    private JSlider supplierAccessoriesIntervalSlider;
    private JSpinner storageCarCapacitySpinner;
}
