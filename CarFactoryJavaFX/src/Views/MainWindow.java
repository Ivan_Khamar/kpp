package Views;

import ConfigurationManager.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindow extends JFrame {

    public MainWindow() {
        optionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ConfigurationSettings CS1 = new ConfigurationSettings();
                    CS1.setVisible(true);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        });

        setContentPane(MainPanel);
        setVisible(true);

        setMinimumSize(new Dimension(400, 435));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    static boolean isInitialised = false;
    static public Configuration configuration;

    private JPanel MainPanel;
    private JButton startEmulateButton;
    private JButton optionsButton;
}