package Views;

import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.swing.JButton;
import javax.swing.SwingUtilities;

import java.awt.event.ActionListener;
import java.io.IOException;

public class MainWindowController {
    public void pressButton(ActionEvent event)
    {
        System.out.println("Hello world");
    }

    @FXML
    public void GetOptions(ActionEvent event) throws IOException, ClassNotFoundException {
        ConfigurationSettings CS1 = new ConfigurationSettings();
        CS1.setVisible(true);
        
    }

    private void createSwingContent(final SwingNode swingNode) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                swingNode.setContent(new JButton("Click me!"));
            }
        });
    }
}
