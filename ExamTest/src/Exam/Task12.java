package Exam;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Task12 {
    static void distinct(int arr[], int num)
    {
        System.out.println("Unique numbers: ");
        for (int i = 0; i < num; i++)
        {
            int j;
            for (j = 0; j < i; j++)
                if (arr[i] == arr[j])
                    break;
            if (i == j)
                System.out.print(arr[i] + " ");
        }
    }

    static void distinctList(ArrayList<Integer> arrList)
    {
        int len = arrList.size();
        System.out.println("Unique numbers: ");
        for (int i = 0; i < len; i++)
        {
            int j;
            for (j = 0; j < i; j++)
                if (arrList.get(i) == arrList.get(j))
                    break;
            if (i == j)
                System.out.print(arrList.get(i) + " ");
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        /*ArrayList<String> data = new ArrayList<String>();
        File myObj = new File("E:\\Intellij\\JavaProjects\\ExamTest\\src\\Ticket4\\K1.txt");
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            data.add(myReader.nextLine());

        }
        myReader.close();
        System.out.println(data);
        String[] Digit = data.get(0).split(" ");
        int[] ret = new int[Digit.length];
        for (int i=0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(Digit[i]);
        }*/
        //distinct(ret, ret.length);

        ArrayList<Integer> arrList = new ArrayList<>();
        arrList.add(1);
        arrList.add(2);
        arrList.add(3);
        arrList.add(3);
        distinctList(arrList);
    }
}
