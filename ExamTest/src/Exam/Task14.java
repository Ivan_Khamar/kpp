package Exam;

import java.util.*;

public class Task14 {
    public static void mapUsage()
    {
        Map<Integer, java.util.List<String>> states = new HashMap<Integer, java.util.List<String>>();
        java.util.List<String> valuesList1 = Arrays.asList("Deutch", "Germany", "Німеччина");
        java.util.List<String> valuesList2 = Arrays.asList("Großbritannien", "Britain", ",Британія");
        java.util.List<String> valuesList3 = Arrays.asList("Ukraine", "Ukraine", "Україна");
        Collections.reverse(valuesList1);
        Collections.reverse(valuesList2);
        Collections.reverse(valuesList3);
        states.put(1, valuesList1);
        states.put(2, valuesList2);
        states.put(4, valuesList3);

        System.out.println(states.get(1));
        System.out.println(states.get(2));
        System.out.println(states.get(4));
    }

    public static void main(String args[])
    {
        mapUsage();
    }
}
