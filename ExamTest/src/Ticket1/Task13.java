package Ticket1;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Task13 {
    public static void dateMethod()throws Exception
    {
        System.out.println("Enter date in dd/MM/yyyy format: \n");
        Scanner keyboard = new Scanner(System.in);
        String myInput = keyboard.next();
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(myInput);
        String dt = myInput;  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(dt));
        c.add(Calendar.DATE, 1);  // number of days to add
        dt = sdf.format(c.getTime());  // dt is now the new date
        System.out.println(myInput+"\t"+dt);
    }
    public static void main(String[] args) {
        System.out.println("Hello !");
        try {
            dateMethod();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
