package Ticket1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Task14 {
    public static void mapUsage()
    {
        Map<Integer, String> states = new HashMap<Integer, String>();
        List<String> valuesList1 = Arrays.asList("Deutch", "Germany", "Німеччина");
        List<String> valuesList2 = Arrays.asList("Großbritannien", "Britain", ",Британія");
        List<String> valuesList3 = Arrays.asList("Ukraine", "Ukraine", "Україна");
        states.put(1, valuesList1.get(0));
        states.put(2, valuesList2.get(0));
        states.put(4, valuesList3.get(0));
        // отримаємо елемент за ключем 1
        String first = states.get(1);
        System.out.println(first);
    }
    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello !");
        mapUsage();
    }
}
