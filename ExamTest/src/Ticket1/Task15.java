package Ticket1;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

class testMain implements java.io.Serializable
{
    public String a;
    public String b;

    // Default constructor
    public testMain(String a, String b)
    {
        this.a = a;
        this.b = b;
    }
}


public class Task15 {
    public static void main(String[] args) throws ParseException {
        System.out.println("Enter date in dd/MM/yyyy format: \n");
        Scanner keyboard = new Scanner(System.in);
        String myInput = keyboard.next();
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(myInput);
        String dt = myInput;  // Start date
        System.out.println("Enter person name: \n");
        String myInput2 = keyboard.next();
        String name = myInput2;  // Start date
        testMain object = new testMain(name, dt);
        String filename = "file.ser";

        // Serialization
        try
        {
            //Saving of object in a file
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            // Method for serialization of object
            out.writeObject(object);

            out.close();
            file.close();

            System.out.println("Object has been serialized");

        }

        catch(IOException ex)
        {
            System.out.println("IOException is caught");
        }


        testMain object1 = null;

        // Deserialization
        try
        {
            // Reading the object from a file
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialization of object
            object1 = (testMain)in.readObject();

            in.close();
            file.close();

            System.out.println("Object has been deserialized ");
            System.out.println("Name of person = " + object1.a);
            System.out.println("Date = " + object1.b);
        }

        catch(IOException ex)
        {
            System.out.println("IOException is caught");
        }

        catch(ClassNotFoundException ex)
        {
            System.out.println("ClassNotFoundException is caught");
        }
    }
}
