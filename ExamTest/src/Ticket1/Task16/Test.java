package Ticket1.Task16;

import java.io.*;
import java.util.LinkedList;

public class Test {
    public static void readWithBufferedThreads() throws IOException {
        BufferedReader inputStream = null;
        PrintWriter outputStream = null;
        LinkedList<String> stringsCollection = new LinkedList<String>();
        try {
            inputStream = new BufferedReader(new FileReader("E:\\Intellij\\JavaProjects\\ExamTest\\src\\Ticket1.Task16\\text.txt"));
            outputStream = new PrintWriter(new FileWriter("E:\\Intellij\\JavaProjects\\ExamTest\\src\\Ticket1.Task16\\outputText.txt"));

            String l;
            String rez = new String();
            //тепер читаємо дані цілими рядками
            while ((l = inputStream.readLine()) != null) {
                stringsCollection.add(l);
                String[] rezultStringsArray = l.split(" ");
                rez = rezultStringsArray[0]+"\n";
                //System.out.println(l);

                outputStream.println(rez);
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        readWithBufferedThreads();
    }
}
