package Ticket1.Task17;

class ThreadDemo implements Runnable {
    Thread t;
    String words;
    Integer threadID;
    ThreadDemo(String str, Integer num) {
        t = new Thread(this, "Thread");
        System.out.println("Child thread: " + t);
        this.words = str;
        this.threadID = num;
        t.start();
    }
    public void run() {
        try {
            String[] rezultStringsArray = this.words.split(" ");
            System.out.println("Child Thread");
            if (this.threadID == 1)
            {
                for (int i=0; i< rezultStringsArray.length; i++)
                {
                    if (i % 2 == 0)
                    {
                        Thread.sleep(1);
                        System.out.println(rezultStringsArray[i]);
                    }
                }
            }else
                {
                    for (int i=0; i< rezultStringsArray.length; i++)
                    {
                        if (i % 2 != 0)
                        {
                            Thread.sleep(120);
                            System.out.println(rezultStringsArray[i]);
                        }
                    }
                }
            Thread.sleep(50);
        } catch (InterruptedException e) {
            System.out.println("The child thread is interrupted.");
        }
        System.out.println("Exiting the child thread");
    }
}

public class Test {
    public static void main(String[] args)
    {
        String words = "Here we have some words for output";
        Integer threadId1 = 1;
        Integer threadId2 = 2 ;

        ThreadDemo ThreadDemo1 = new ThreadDemo(words,threadId1);
        ThreadDemo ThreadDemo2 = new ThreadDemo(words,threadId2);
        try {
            System.out.println("Main Thread");
            Thread.sleep(100);
        } catch (InterruptedException e) {
            System.out.println("The Main thread is interrupted");
        }
        System.out.println("Exiting the Main thread");
    }
}
