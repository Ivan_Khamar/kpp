package Ticket2;

import java.util.LinkedList;

public class Task12 {
    public static int[] getNumberReturnReverseAndFirstDigit(int inputNumber)
    {
        int number = inputNumber;
        int firstDigit = Integer.parseInt(Integer.toString(number).substring(0, 1));
        int reverseDigit = 0;

        while(number != 0) {
            int digit = number % 10;
            reverseDigit = reverseDigit * 10 + digit;
            number /= 10;
        }

        int[] rezArray = {reverseDigit,firstDigit};
        return rezArray;
    }
    public static void main(String[] args)
    {
        int[] rezArray = getNumberReturnReverseAndFirstDigit(678);
        for (int i = 0; i< rezArray.length;i++)
        {
            System.out.println(rezArray[i]+" ");
        }
        System.out.println("\n");
    }

}
