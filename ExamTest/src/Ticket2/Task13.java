package Ticket2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Task13 {
    public static void dateMethod()throws Exception
    {
        System.out.println("Enter date in dd/MM/yyyy format: \n");

        Scanner keyboard = new Scanner(System.in);
        String myInput = keyboard.next();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(myInput);
        Date currentDate=java.util.Calendar.getInstance().getTime();
        sdf.format(currentDate);

        if(date1.after(currentDate)) {
            System.out.println("That date will come in future.");
        }
        if (date1.before(currentDate))
        {
            System.out.println("That date already in a past.");
        }
    }
    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello !");
        try {
            dateMethod();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
