package Ticket2;

import java.io.*;
import java.util.*;

public class Task15 {
    public static Map<Integer, String> mapUsage()
    {
        HashMap<Integer, String> states = new HashMap<Integer, String>();
        List<String> valuesList1 = Arrays.asList("Deutch", "Germany", "Німеччина");
        List<String> valuesList2 = Arrays.asList("Großbritannien", "Britain", "Британія");
        List<String> valuesList3 = Arrays.asList("Ukraine", "Ukraine", "Україна");
        states.put(1, valuesList1.get(valuesList1.size()-1));
        states.put(4, valuesList2.get(valuesList2.size()-1));
        states.put(3, valuesList3.get(valuesList3.size()-1));
        return states;
    }

    public static void Serialize(Map<Integer, String> states)
    {
        try
        {
            FileOutputStream fos =
                    new FileOutputStream("E:\\Intellij\\JavaProjects\\ExamTest\\src\\Ticket2\\serialize.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(states);
            oos.close();
            fos.close();
            System.out.printf("Serialized HashMap data is saved in serialize.txt\n");
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    public static void deSerialize()
    {
        HashMap<Integer, String> map = null;
        try
        {
            FileInputStream fis = new FileInputStream("E:\\Intellij\\JavaProjects\\ExamTest\\src\\Ticket2\\serialize.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
        System.out.println("Deserialized HashMap:");
        // Display content using Iterator
        Set set = map.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            System.out.print("key: "+ mentry.getKey() + " & Value: ");
            System.out.println(mentry.getValue());
        }
    }

    public static void main(String[] args) {
        Map<Integer, String> states = mapUsage();
        Serialize(states);
        deSerialize();
    }
}
