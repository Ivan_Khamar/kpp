package Ticket2;

import java.util.stream.*;

public class Task16 {
    static boolean isDigitPresent(int x, int d)
    {
        while (x > 0)
        {
            if (x % 10 == d)
                break;

            x = x / 10;
        }
        return (x > 0);
    }
    public static void main (String[] args)
    {
        int d = 9;
        int[] arr = new int[5];
        int[] arr1 = new int[arr.length];

        arr[0] = 17;
        arr[1] = 39;
        arr[2] = 9;
        arr[3] = 40;
        arr[4] = 52;

        for (int i = 0; i < arr.length; i++)
        {
            if ( isDigitPresent(arr[i], d))
            {
                arr1[i] = arr[i];
            }
        }
        long Sum = IntStream.of(arr1).sum();
        System.out.println(Sum);
    }
}

/*static boolean isDigitPresent(int x, int d)
    {
        while (x > 0)
        {
            if (x % 10 == d)
                break;

            x = x / 10;
        }
        return (x > 0);
    }
    public static void main (String[] args)
    {
        int d = 9;
        int[] arr = new int[5];
        int[] arr1 = new int[arr.length];

        arr[0] = 17;
        arr[1] = 39;
        arr[2] = 9;
        arr[3] = 40;
        arr[4] = 52;

        for (int i = 0; i < arr.length; i++)
        {
            if ( isDigitPresent(arr[i], d))
            {
                arr1[i] = arr[i];
            }
        }
        long Sum = IntStream.of(arr1).sum();
        System.out.println(Sum);
    }*/

