package Ticket2.Task17;

class main
{
    public static void main(String[] args)
    {
        int min = -5;
        int max = 20;
        int[][] a=new int[4][4];
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                a[i][j]= (int)(Math.random() * (max - min + 1) + min);
                System.out.print(a[i][j]+"\t");
            }
            System.out.print("\n");
        }
        Integer threadId1 = 1;
        Integer threadId2 = 2 ;

        ThreadDemo ThreadDemo1 = new ThreadDemo(a,threadId1);
        ThreadDemo ThreadDemo2 = new ThreadDemo(a,threadId2);
    }
}