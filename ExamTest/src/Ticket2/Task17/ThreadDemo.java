package Ticket2.Task17;

import java.util.LinkedList;

class ThreadDemo implements Runnable
{
    Thread t;
    int[][] matrix;
    Integer threadID;
    ThreadDemo(int[][] a, Integer num)
    {
        t = new Thread(this, "Thread");
        System.out.println("Child thread: " + t);
        this.matrix = a;
        this.threadID = num;
        t.start();
    }
    public void run()
    {
        LinkedList<String> mainDiagonal = new LinkedList<>();
        LinkedList<String> secondDiagonal = new LinkedList<>();
        if (this.threadID == 1)
        {
            int mainSum = 0;
            for (int i = 0; i < matrix.length; i++)
            {
                for (int j = 0; j < matrix.length; j++)
                {
                    if (i == j)
                    {
                        if (matrix[i][j] >= 0)
                        {
                            mainSum += matrix[i][j];
                        }
                        else
                        {
                            int realI = i+1;
                            int realJ = j+1;
                            System.out.println("i: " +realI + " j: " +realJ);
                            break;
                        }
                    }
                }
            }
            System.out.println(mainSum);
        }
        if (this.threadID == 2)
        {
            int mainSum = 0;
            for (int i = 0; i < matrix.length; i++)
            {
                for (int j = 0; j < matrix.length; j++)
                {
                    if ((i + j) == (matrix.length - 1))
                    {
                        if (matrix[i][j] >= 0)
                        {
                            mainSum += matrix[i][j];
                        }
                        else
                        {
                            int realI = i+1;
                            int realJ = j+1;
                            System.out.println("i: " +realI + " j: " +realJ);
                            break;
                        }
                    }
                }
            }
            System.out.println("\n" + mainSum);
        }
    }
}
