package Ticket3;

import java.util.*;
import java.util.List;

class thisMain
{
    public static void main(String args[])
    {
        //implementation of HashMap
        HashMap<Integer, List<String>> hm=new HashMap<>();
        //esList1 = Arrays.asList("Deutch", "Germany", "Німеччина");
        java.util.List<String> valuesList1 = Arrays.asList("Deutch", "Germany", "Німеччина");
        java.util.List<String> valuesList2 = Arrays.asList("Großbritannien", "Britain", ",Британія");
        java.util.List<String> valuesList3 = Arrays.asList("Ukraine", "Ukraine", "Україна");
        hm.put(23, valuesList1);
        hm.put(17, valuesList2);
        hm.put(15, valuesList3);
        Iterator <Integer> it = hm.keySet().iterator();
        System.out.println("Before Sorting");
        while(it.hasNext())
        {
            int key=(int)it.next();
            System.out.println("Roll no:  "+key+"     name:   "+hm.get(key));
        }
        System.out.println("\n");
        Map<Integer, String> map=new HashMap<Integer, String>();
        System.out.println("After Sorting");
        //using TreeMap constructor to sort the HashMap
        TreeMap<Integer, List<String>> tm=new  TreeMap<Integer, List<String>> (hm);
        Iterator itr=tm.keySet().iterator();
        while(itr.hasNext())
        {
            int key=(int)itr.next();
            System.out.println("Roll no:  "+key+"     name:   "+hm.get(key));
        }
    }
}

