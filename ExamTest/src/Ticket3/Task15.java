package Ticket3;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class Task15 {

    static class person implements Serializable {
        String personName;
        LocalDate personDate;

        public person() {
        }
    }

    public static LinkedList<person> makingList()
    {
        LinkedList<person> people = new LinkedList<person>();
        person person1 = new person();
        person1.personName = "Oleg";
        person1.personDate = LocalDate.of(2020, 8, 5);
        people.add(person1);
        person person2 = new person();
        person2.personName = "Petro";
        person2.personDate = LocalDate.of(2020,8,1);
        people.add(person2);

        return people;
    }

    public static LinkedList<person> sortingList(LinkedList<person> people)
    {
        for (int i=0; i< people.size();i++)
        {
            System.out.println(people.get(i).personDate+"\n");
        }
        for (int i=0; i< people.size();i++)
        {
            if ((i-1)>=0)
            {
                person tempPerson = new person();
                if (people.get(i-1).personDate.isAfter(people.get(i).personDate)){
                    tempPerson = people.get(i-1);
                    people.remove(i-1);
                    people.add(i,tempPerson);
                    //people.get(i+1) = tempPerson;
                }
            }
        }
        LinkedList<person> sortedPeople = people;
        for (int i=0; i< sortedPeople.size();i++)
        {
            System.out.println(sortedPeople.get(i).personDate+"\n");
        }
        return sortedPeople;
    }

    public static void Serialize(LinkedList<person> sortedPeople)
    {
        try
        {
            FileOutputStream fos =
                    new FileOutputStream("E:\\Intellij\\JavaProjects\\ExamTest\\src\\Ticket3\\serialize.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(sortedPeople);
            oos.close();
            fos.close();
            System.out.printf("Serialized HashMap data is saved in serialize.txt\n");
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args) {
        LinkedList<person> people = makingList();
        LinkedList<person> sortedPeople = sortingList(people);

        Serialize(sortedPeople);
    }
}
