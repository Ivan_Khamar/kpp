package Ticket3;
import java.util.stream.*;

public class Task16 {
    static class GFG
    {
        static boolean isDigitPresent(String x)
        {
            boolean std = false;
            char[] stringLetters = new char[x.length()];
            for (int i = 0; i < x.length(); i++) {
                stringLetters[i] = x.charAt(i);
            }
            if (stringLetters[0] == stringLetters[stringLetters.length-1])
            {
                std = true;
            }

            return std;
        }

        public static void main (String[] args)
        {
            //Integer d = 9;
            // declares an Array of integers.
            String[] arr;
            int[] arr1;

            // allocating memory for 5 integers.
            arr = new String[5];
            arr1 =  new int[arr.length];

            // initialize the first elements of the array
            arr[0] = "aristotel";

            // initialize the second elements of the array
            arr[1] = "philip";

            //so on...
            arr[2] = "mykola";
            arr[3] = "anna";
            arr[4] = "pavlo";

            // accessing the elements of the specified array
            for (int i = 0; i < arr.length; i++)
            {
                if ( isDigitPresent(arr[i]))
                {
                    arr1[i] = 1;
                }
            }
            long Count = IntStream.of(arr1).sum();
            System.out.println(Count);
        }
    }
}