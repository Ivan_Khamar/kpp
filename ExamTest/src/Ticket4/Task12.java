package Ticket4;

import java.util.LinkedList;

public class Task12 {
    public static int[] getNumberReturnDigitsCountAndMultiplies(int inputNumber)
    {
        int number = inputNumber; // = and int
        LinkedList<Integer> stack = new LinkedList<Integer>();
        LinkedList<Integer> rezStack = new LinkedList<Integer>();;
        while (number > 0) {
            stack.push( number % 10 );
            number = number / 10;
        }

        int Sum = 0;
        int digitsCount = 0;

        for (int i=0;i<stack.size();i++)
        {
            digitsCount = i+1;
            Sum += stack.get(i);
        }
        int[] rezArray = {digitsCount,Sum};
        return rezArray;
    }
    public static void main(String[] args)
    {
        int[] rezArray = getNumberReturnDigitsCountAndMultiplies(228);
        for (int i=0; i < rezArray.length; i++)
        {
            System.out.println(rezArray[i]);
        }
        //System.out.println(getNumberReturnDigitsCountAndMultiplies(123));
    }

}
