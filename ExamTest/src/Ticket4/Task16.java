package Ticket4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task16 {
    public static void main(String[]args)
    {
        String str = "string1234more567string890";
        System.out.println(str);
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(str);
        while(m.find())
        {
            System.out.println(m.group());
        }
        System.out.println(removeAllDigit(str));
    }
    public static String removeAllDigit(String str)
    {
        // Converting the given string
        // into a character array
        char[] charArray = str.toCharArray();
        String result = "";

        // Traverse the character array
        for (int i = 0; i < charArray.length; i++) {

            // Check if the specified character is not digit
            // then add this character into result variable
            if (!Character.isDigit(charArray[i])) {
                result = result + charArray[i];
            }
        }
        // Return result
        return result;
    }

}
