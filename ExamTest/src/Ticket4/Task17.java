package Ticket4;

import java.util.ArrayList;

public class Task17 {
    public static void main (String[] args) throws InterruptedException {
        // declares an Array of integers.
        int[] arr;

        // allocating memory for 10 integers.
        arr = new int[10];

        // initialize elements of the array
        arr[0] = 100;
        arr[1] = 50;
        arr[2] = 40;
        arr[3] = 40;
        arr[4] = 50;
        arr[5] = 70;
        arr[6] = 30;
        arr[7] = 80;
        arr[8] = 60;
        arr[9] = 90;

        ArrayList<Integer> kpp = new ArrayList<Integer>();
        ArrayList<Integer> kpp2 = new ArrayList<Integer>();
        ArrayList<Integer> kpp3 = new ArrayList<Integer>();

        for (int i = 0; i < arr.length; i++)
        {
            if (i<3)
            {
                kpp.add(arr[i]);
            }
            if (i<5 & i>=3)
            {
                kpp2.add(arr[i]);
            }
            if (i<10 & i>=5)
            {
                kpp3.add(arr[i]);
            }
        }
        ThreadDemo ThreadDemo1 = new ThreadDemo(kpp);
        ThreadDemo ThreadDemo2 = new ThreadDemo(kpp2);
        ThreadDemo ThreadDemo3 = new ThreadDemo(kpp3);
        Thread.sleep(1000);
        int K = ThreadDemo1.suma;
        int KK = ThreadDemo2.suma;
        int KKK = ThreadDemo3.suma;
        long Sum = K + KK + KKK;
        //System.out.println(K);
        //System.out.println(KK);
        //System.out.println(KKK);
        System.out.println(Sum);
    }

}
class ThreadDemo implements Runnable
{
    Thread t;
    ArrayList<Integer> MMM;
    int suma = 0;
    ThreadDemo(ArrayList<Integer> kpp)
    {
        t = new Thread(this, "Thread");
        System.out.println("Child thread: " + t);
        this.MMM = kpp;
        t.start();
    }
    public void run()
    {
        int sum = 0;
        for (int i=0; i<this.MMM.size(); i++)
        {
            sum += this.MMM.get(i);
        }
        this.suma = sum;
    }
}

