package BuyClothes;

public class Customer {
    private Integer customerID;
    private String password;
    private Address address;
    private String email;
    enum CustomerStatus {
        NeW,
        buying,
        trying_on,
        waiting,
        processed
    };

    public void addAddress(Address newAddress) {
        this.address = newAddress;
    }

    public void deleteAddress() {
        this.address = null;
    }

    public void changeEmail(String newMail) {
        this.email = newMail;
    }

    public void changePassword(String newPass) {
        this.password = newPass;
    }
}
