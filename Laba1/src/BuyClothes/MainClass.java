package BuyClothes;

import javax.management.DescriptorRead;
import java.util.*;

abstract class AnonymousInner {
    public abstract void mymethod();
}

public class MainClass {

    public static void main(String[] args) {
        System.out.println("Hello world! This is clothes shop\n");

        List<Product> products = new LinkedList();

        Dress dress1 = new Dress();
        dress1.description = "Dress 1";
        dress1.isOfficial = true;
        dress1.size = 50;
        dress1.manufacturer = "Staff";
        products.add(dress1);

        Dress dress2 = new Dress();
        dress2.description = "Dress 2";
        dress2.isOfficial = false;
        dress2.size = 46;
        dress2.manufacturer = "SpecTacGear";
        products.add(dress2);

        Jacket jacket1 = new Jacket();
        jacket1.description = "Jacket 1";
        jacket1.isOfficial = true;
        jacket1.recomendedPrice = 760;
        jacket1.actualPrice = 800;
        jacket1.manufacturer = "MTac";
        products.add(jacket1);

        //System.out.println(dress2.isOfficial);
        //System.out.println(products.get(1).isOfficial);
        AnonymousInner inner = new AnonymousInner() {
            public void mymethod() {
                ProductManager manager = new ProductManager();
                System.out.println("Here is anonymous official clothes: ");
                manager.findOfficial(products);
            }
        };
        inner.mymethod();

        System.out.println("\nHere is inner size comparator: ");
        Collections.reverse(products);
        ProductManager manager = new ProductManager();
        if (manager.sortBySize(dress1,dress2) > 0)
        {
            System.out.println(dress1.description+" size is bigger than "+dress2.description+" size");
        }
        if (manager.sortBySize(dress1,dress2) < 0)
        {
            System.out.println(dress2.description+" size is bigger than "+dress1.description+" size");
        }
        if (manager.sortBySize(dress1,dress2) == 0)
        {
            System.out.println(dress1.description+" size is equal as "+dress2.description+" size");
        }

        System.out.println("\nHere is static inner price comparable: ");
        //System.out.println(manager.comparePrices(jacket1));
        Collections.reverse(products);
        if (manager.comparePrices(jacket1) > 0)
        {
            System.out.println(jacket1.description+" actual price is lower than "
                    +jacket1.description+" recommended price");
        }
        if (manager.comparePrices(jacket1) < 0)
        {
            System.out.println(jacket1.description+" actual price is higher than "
                    +jacket1.description+" recommended price");
        }
        if (manager.comparePrices(jacket1) == 0)
        {
            System.out.println(jacket1.description+" actual price is equal as "
                    +jacket1.description+" recommended price");
        }

        System.out.println("\nHere is lambda staff brand clothes: ");
        manager.sortByBrand(products, "Staff");

        System.out.println("\nEnjoy your new look)");
    }
}