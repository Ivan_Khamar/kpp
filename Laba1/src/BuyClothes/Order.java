package BuyClothes;

import java.util.Date;

public class Order {
    private String orderID;
    private Date orderDate;
    private Double sum;
    public enum OrderStatus {
        NeW,
        processing,
        delivering,
        delivered,
        canceled
    };
    public Order(
            String orderID,
            Date orderDate,
            Double sum,
            Enum OrderStatus
    ){
        this.orderID = orderID;
        this.orderDate = orderDate;
        this.sum = sum;
    }
    public Bill createBillInfo()
    {
        return new Bill();
    }

    public Void updateOrder(Order order, String Variant )
    {
        return null;
    }
}
