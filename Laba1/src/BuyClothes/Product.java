package BuyClothes;

public class Product extends Item {
    public Integer productID;
    public String description;
    public String manufacturer;
    public int actualPrice;
    public Integer size;
    public String colour;
    public boolean isOfficial;
}
