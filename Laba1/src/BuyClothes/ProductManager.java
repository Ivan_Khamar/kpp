package BuyClothes;

import java.util.Comparator;
import java.util.List;

public class ProductManager extends Product {

    private class KomparatorI implements Comparator<Product> {
        public int compare(Product a1, Product a2) {
            return Integer.compare(a1.size, a2.size);
        }
    }

    public static class KomparableI implements Comparable<Product>{
        public int compareTo(Product o) {
            return Integer.compare(o.recomendedPrice, o.actualPrice);
        }
    }

    public List<Product> findOfficial(List<Product> productList)
    {
        for (Product prod: productList)
        {
            if (prod.isOfficial == true)
            {
                System.out.println(prod.description);
            }
        }
        return productList;
    }
    
    public int sortBySize(Product firProd, Product secProd)
    {
        KomparatorI komparator = new KomparatorI();
        int compare = komparator.compare(firProd, secProd);
        return compare;
    }

    public int comparePrices(Product prod)
    {
        KomparableI komparable = new KomparableI();
        int compare = komparable.compareTo(prod);
        return compare;
    }

    public void sortByBrand(List<Product> productList, String brand)
    {
        productList.forEach(
                (prod) -> {
                    if (prod.manufacturer == brand) {System.out.println(prod.description);}
                }
        );
    }
}
