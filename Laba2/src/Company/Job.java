package Company;

import java.util.HashMap;
import java.util.Map;

public enum Job {
    JUNIOR(0),
    MIDDLE(1),
    SENIOR(2);

    private  int value;
    private static Map map = new HashMap<>();

    Job(int value) {
        this.value = value;
    }

    static {
        for (Job jobType : Job.values()) {
            map.put(jobType.value, jobType);
        }
    }

    public static Job valueOf(int jobType) {
        return (Job) map.get(jobType);
    }

    public int getValue() {
        return value;
    }

    public static int getCount(){
        return map.size();
    }

}
