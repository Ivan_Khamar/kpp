package Company;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class MainClass {

    public static void sortHashMapEmployees(HashMap<Date,ArrayList<employeeInfo>> employeesInfo){
        for (Map.Entry<Date, ArrayList<employeeInfo>> entry : employeesInfo.entrySet()) {
            entry.getValue().sort(new employeeInfo.JobTypeComparator());
        }
    }

    public static void ShowEmployees(ArrayList<employeeInfo> employeesInfo){
        for (employeeInfo p: employeesInfo) {
            System.out.println("\t" + p.toString());
        }
    }

    public static HashMap<Date,ArrayList<employeeInfo>> CreateMap(ArrayList<employeeInfo> employeesInfo){
        HashMap<Date,ArrayList<employeeInfo>> hashMap = new HashMap<>();

        for (employeeInfo p : employeesInfo) {
            hashMap.put(p.assignmentDate,new ArrayList<>());
        }
        for (employeeInfo p : employeesInfo) {
            hashMap.get(p.assignmentDate).add(p);
        }

        return hashMap;
    }

    public static void ShowHashMap(HashMap<Date,ArrayList<employeeInfo>> employeesInfo){

        for (Map.Entry<Date, ArrayList<employeeInfo>> entry : employeesInfo.entrySet()) {
            System.out.println(entry.getKey());
            ShowEmployees(entry.getValue());
        }
    }

    public static void RemoveEmployees(HashMap<Date,ArrayList<employeeInfo>> employeesInfo) {
        ArrayList<Date> toRemove = new ArrayList<>();
        for (Map.Entry<Date, ArrayList<employeeInfo>> entry : employeesInfo.entrySet()) {
            var employeeDate = entry.getKey();

            DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            LocalDate  d1 = LocalDate.parse(formatter.format(employeeDate), df);
            LocalDate  d2 = LocalDateTime.now().toLocalDate();

            Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
            long diffYears = diff.toDays() / 365;
            if(diffYears > 10){
                toRemove.add(employeeDate);
            }
        }
        for(int i = 0;i< toRemove.size();i++){
            employeesInfo.remove(toRemove.get(i));
        }
    }

    public static ArrayList<employeeInfo> MergeCollection(ArrayList<employeeInfo> listFromFirstFile, ArrayList<employeeInfo> listFromSecondFile){
        ArrayList<employeeInfo> mergedList = new ArrayList<>();
        AddUniqueEmployeeToCollection(listFromFirstFile, mergedList);
        AddUniqueEmployeeToCollection(listFromSecondFile, mergedList);
        return mergedList;
    }

    private static void AddUniqueEmployeeToCollection(ArrayList<employeeInfo> listFromFirstFile, ArrayList<employeeInfo> res) {
        for (employeeInfo firstListEmployee: listFromFirstFile){
            boolean exist = false;
            for (employeeInfo resEmployee: res){
                if(resEmployee.surname.equals(firstListEmployee.surname)){
                    exist = true;
                    break;
                }
            }
            if (!exist){
                res.add(firstListEmployee);
            }
            exist = false;
        }
    }

    public static void GetJobCounts(ArrayList<employeeInfo> list){
        int[] array = new int[Job.getCount()];
        for (int i = 0; i < Job.getCount(); i++)
        {
            array[i] = 0;
        }

        for (int i = 0; i < Job.getCount(); i++){
            for (employeeInfo p : list){
                if(p.jobType.equals(Job.valueOf(i))){
                    array[i]++;
                }
            }
        }

        for (int i = 0; i < Job.getCount(); i++){
            System.out.println(Job.valueOf(i) + "  " + array[i]);
        }
    }

    public static void main(String[] args) {

        ArrayList<employeeInfo> employeesInfo;
        HashMap<Date,ArrayList<employeeInfo>> hashMap = new HashMap<>();
        ArrayList<employeeInfo> listFromFirstFile = new ArrayList<>();
        ArrayList<employeeInfo> listFromSecondFile = new ArrayList<>();
        ArrayList<employeeInfo> mergedList = new ArrayList<>();

        Scanner scannedInput = new Scanner(System.in);

        while (true){
            System.out.println("1 - Зчитати з файлу та сформувати карту");
            System.out.println("2 - Відсортувати карту");
            System.out.println("3 - Вилучити всіх осіб, для яких на біжучу дату зарахування відбулось більше 10 років назад");
            System.out.println("4 - Зчитати з 2 файлів");
            System.out.println("5 - Вивести дві колекції");
            System.out.println("6 - Сформувати колекцію в якій немає повторів прізвищ");
            System.out.println("7 - Вивести частотну таблицю посад з колекції");
            System.out.println("0 - Вихід");
            System.out.println("\nInput a number: ");
            int enteredNumber = scannedInput.nextInt();

            switch (enteredNumber) {
                case 1 -> {
                    employeesInfo = employeeInfoReader.readEmployeesInfo("E:\\Intellij\\JavaProjects\\Laba2\\src\\employees.txt");
                    hashMap = CreateMap(employeesInfo);
                    System.out.println("RESULT");
                    ShowHashMap(hashMap);
                    System.out.println("-------------------------------------");
                }
                case 2 -> {
                    sortHashMapEmployees(hashMap);
                    System.out.println("RESULT");
                    ShowHashMap(hashMap);
                    System.out.println("-------------------------------------");
                }
                case 3 -> {
                    RemoveEmployees(hashMap);
                    System.out.println("RESULT");
                    ShowHashMap(hashMap);
                    System.out.println("-------------------------------------");
                }
                case 4 -> {
                    listFromFirstFile = employeeInfoReader.readEmployeesInfo("E:\\Intellij\\JavaProjects\\Laba2\\src\\employees.txt");
                    listFromSecondFile = employeeInfoReader.readEmployeesInfo("E:\\Intellij\\JavaProjects\\Laba2\\src\\altEmployees.txt");
                    System.out.println("READED SUCCESFULLY");
                    System.out.println("-------------------------------------");
                }
                case 5 -> {
                    System.out.println("RESULT");
                    System.out.println("\tLIST FROM FIRST FILE");
                    ShowEmployees(listFromFirstFile);
                    System.out.println("\n\tLIST FROM SECOND FILE");
                    ShowEmployees(listFromFirstFile);
                    System.out.println("-------------------------------------");
                }
                case 6 -> {
                    mergedList = MergeCollection(listFromFirstFile, listFromSecondFile);
                    System.out.println("RESULT");
                    ShowEmployees(mergedList);
                    System.out.println("-------------------------------------");
                }
                case 7 -> {
                    GetJobCounts(mergedList);
                }
            }
            if(enteredNumber == 0){
                break;
            }
        }

    }

}
