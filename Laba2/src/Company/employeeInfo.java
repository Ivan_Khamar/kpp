package Company;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class employeeInfo {
    public String surname;
    public Date assignmentDate;
    public Job jobType;

    public employeeInfo(String surname, Date assignmentDate, Job jobType) {
        this.surname = surname;
        this.assignmentDate = assignmentDate;
        this.jobType = jobType;
    }
    public employeeInfo() {
        this.surname = "";
        this.assignmentDate = null;
        this.jobType = null;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return "EmployeeInfo{" +
                "surname='" + surname + '\'' +
                ", assignmentDate=" + formatter.format(assignmentDate) +
                ", jobType=" + jobType +
                '}';
    }

    public static class JobTypeComparator implements Comparator<employeeInfo> {
        public int compare(employeeInfo p1, employeeInfo p2){
            return Integer.compare(p1.jobType.getValue(),p2.jobType.getValue());
        }
    }

}
