package Company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class employeeInfoReader {

    public static ArrayList<employeeInfo> readEmployeesInfo(String path){
        ArrayList<employeeInfo> resList = new ArrayList<>();

        String line;
        try {
            BufferedReader bufferReader = new BufferedReader(new FileReader(path));
            while ((line = bufferReader.readLine()) != null) {
                resList.add(parseEmployeeInfo(line));
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resList;
    }

    private static employeeInfo parseEmployeeInfo(String line) throws ParseException {
        employeeInfo employeeInfo = new employeeInfo();
        String array[] = line.split(" ");

        employeeInfo.surname = array[0];
        employeeInfo.assignmentDate = new SimpleDateFormat("dd/MM/yyyy").parse(array[1]);
        employeeInfo.jobType = Job.valueOf(array[2]);
        return employeeInfo;
    }
}
