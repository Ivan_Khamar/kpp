package com.example.laba3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.lang.String;

public class Laba3 {

    public Laba3() {

    }

    public static String readFileByScanner() throws FileNotFoundException {
        String fileContent = "";
        File sampleFile = new File("E:\\Intellij\\JavaProjects\\Laba3\\src\\com\\example\\laba3\\text.txt");
        Scanner scan = new Scanner(sampleFile);
        while (scan.hasNextLine()){
            fileContent = fileContent.concat(scan.nextLine() + "\n");
        }
        return fileContent;
    }

    public static String readUkrFileByScanner() throws FileNotFoundException {
        String fileContent = "";
        File sampleFile = new File("E:\\Intellij\\JavaProjects\\Laba3\\src\\com\\example\\laba3\\text_ukr.txt");
        Scanner scan = new Scanner(sampleFile);
        while (scan.hasNextLine()){
            fileContent = fileContent.concat(scan.nextLine() + "\n");
        }
        return fileContent;
    }

    public static String spacesReplace(String fileContent)
    {
        String fileContentSpacesReplaced = fileContent.replace("    "," ");
        fileContentSpacesReplaced = fileContentSpacesReplaced.replace("   "," ");
        fileContentSpacesReplaced = fileContentSpacesReplaced.replace("  "," ");
        return fileContentSpacesReplaced;
    }

    public static ArrayList<Integer> createSignIndexes(String fileContentSpacesReplaced, Character sign)
    {
        ArrayList<Character> symbols = new ArrayList<Character>();
        ArrayList<Integer> signIndexes = new ArrayList<Integer>();

        for(int i = 0; i < fileContentSpacesReplaced.length(); i++)
        {
            Character c = fileContentSpacesReplaced.charAt(i);
            symbols.add(c);
            boolean symbolMatches  = Pattern.matches(".s",
                    String.valueOf(c));
            if (c == sign || symbolMatches)
            {
                signIndexes.add(i);
            }
        }
        return signIndexes;
    }

    public static ArrayList<Integer> createUkrSignIndexes(String fileUkrContentSpacesReplaced, Character sign)
    {
        ArrayList<Character> symbols = new ArrayList<Character>();
        ArrayList<Integer> signUkrIndexes = new ArrayList<Integer>();

        for(int i = 0; i < fileUkrContentSpacesReplaced.length(); i++)
        {
            Character c = fileUkrContentSpacesReplaced.charAt(i);
            symbols.add(c);
            if (c == sign)
            {
                signUkrIndexes.add(i);
            }
        }
        return signUkrIndexes;
    }

    public static ArrayList<String> createSelectedLexemes(ArrayList<Integer> quotationIndexes, String fileContentSpacesReplaced)
    {
        ArrayList<String> quotatedLexemes = new ArrayList<String>();

        for (int j = 0; j < quotationIndexes.size(); j++)
        {
            if(j+1 < quotationIndexes.size())
            {
                if ((fileContentSpacesReplaced.substring(quotationIndexes.get(j)+1, quotationIndexes.get(j+1)).toString()) != ""){
                quotatedLexemes.add(fileContentSpacesReplaced.substring(quotationIndexes.get(j)+1, quotationIndexes.get(j+1)));
                }
            }
        }
        return quotatedLexemes;
    }

    public static ArrayList<String> createUkrSelectedLexemes(ArrayList<Integer> quotationUkrIndexes, String fileUkrContentSpacesReplaced)
    {
        ArrayList<String> quotatedUkrLexemes = new ArrayList<String>();

        for (int j = 0; j < quotationUkrIndexes.size(); j++)
        {
            if(j+1 < quotationUkrIndexes.size())
            {
                if ((fileUkrContentSpacesReplaced.substring(quotationUkrIndexes.get(j)+1, quotationUkrIndexes.get(j+1)).toString()) != ""){
                    quotatedUkrLexemes.add(fileUkrContentSpacesReplaced.substring(quotationUkrIndexes.get(j)+1, quotationUkrIndexes.get(j+1)));
                }
            }
        }
        return quotatedUkrLexemes;
    }

    public static List<Map.Entry<String, Long>> createQuotationRating(ArrayList<String> quotatedLexemes)
    {
        Map<String, Long> map = quotatedLexemes.stream()
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()));

        List<Map.Entry<String, Long>> result = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(quotatedLexemes.size())
                .collect(Collectors.toList());
        return result;
    }

    public static List<Map.Entry<String, Long>> createUkrQuotationRating(ArrayList<String> quotatedUkrLexemes)
    {
        Map<String, Long> map = quotatedUkrLexemes.stream()
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()));

        List<Map.Entry<String, Long>> result = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(quotatedUkrLexemes.size())
                .collect(Collectors.toList());
        return result;
    }

    public static ArrayList<String> replaceWithPopular(ArrayList<String> quotatedLexemes, List<Map.Entry<String, Long>> result)
    {
        ArrayList<String> perelic = new ArrayList<>(Arrays.asList("dress", "handling", "car"));
        for (int i = 0; i < quotatedLexemes.size(); i++)
        {
            if (quotatedLexemes.get(i).equals(perelic.get(0)) || quotatedLexemes.get(i).equals(perelic.get(1)) || quotatedLexemes.get(i).equals(perelic.get(2)))
            {
                quotatedLexemes.set(i, result.get(0).toString().substring(0, result.get(0).toString().length() - 2));
            }
        }
        return quotatedLexemes;
    }

    public static ArrayList<String> replaceUkrWithPopular(ArrayList<String> quotatedUkrLexemes, List<Map.Entry<String, Long>> result2)
    {
        ArrayList<String> perelic = new ArrayList<>(Arrays.asList("плаття", "обробки", "авто"));
        for (int i = 0; i < quotatedUkrLexemes.size(); i++)
        {
            if (quotatedUkrLexemes.get(i).equals(perelic.get(0)) || quotatedUkrLexemes.get(i).equals(perelic.get(1)) || quotatedUkrLexemes.get(i).equals(perelic.get(2)))
            {
                quotatedUkrLexemes.set(i, result2.get(0).toString().substring(0, result2.get(0).toString().length() - 2));
            }
        }
        return quotatedUkrLexemes;
    }

    public static ArrayList<String> concatLexemes(ArrayList<String> questionSentencesLexemes, String addedString)
    {
        for (int i = 0; i < questionSentencesLexemes.size(); i++)
        {
            String lexemeElement = questionSentencesLexemes.get(i).concat(addedString);
            questionSentencesLexemes.set(i,lexemeElement);
        }
        return questionSentencesLexemes;
    }

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println(readFileByScanner());
        System.out.println(readUkrFileByScanner());

        String fileContentSpacesReplaced = spacesReplace(readFileByScanner());
        String fileUkrContentSpacesReplaced = spacesReplace(readUkrFileByScanner());

        System.out.println(fileContentSpacesReplaced);
        System.out.println(fileUkrContentSpacesReplaced);

        fileContentSpacesReplaced = fileContentSpacesReplaced.replaceAll("\n"," ");
        fileUkrContentSpacesReplaced = fileUkrContentSpacesReplaced.replaceAll("\n"," ");
        Character sign = '"';
        ArrayList<Integer> quotationIndexes = createSignIndexes(fileContentSpacesReplaced,sign);
        ArrayList<Integer> quotationUkrIndexes = createSignIndexes(fileUkrContentSpacesReplaced,sign);
        //System.out.println(quotationIndexes.toString());
        //System.out.println(quotationUkrIndexes.toString());

        ArrayList<String> quotatedLexemes = createSelectedLexemes(quotationIndexes,fileContentSpacesReplaced);
        ArrayList<String> quotatedUkrLexemes = createUkrSelectedLexemes(quotationUkrIndexes,fileUkrContentSpacesReplaced);
        System.out.println(quotatedLexemes);
        System.out.println(quotatedUkrLexemes);

        List<Map.Entry<String, Long>> result = createQuotationRating(quotatedLexemes);
        List<Map.Entry<String, Long>> result2 = createUkrQuotationRating(quotatedUkrLexemes);
        quotatedLexemes = replaceWithPopular(quotatedLexemes,result);
        quotatedUkrLexemes = replaceUkrWithPopular(quotatedUkrLexemes,result2);
        System.out.println(quotatedLexemes);
        System.out.println(quotatedUkrLexemes);

        Character sign2 = '?';
        ArrayList<Integer> questionIndexes = createSignIndexes(fileContentSpacesReplaced,sign2);
        ArrayList<Integer> questionUkrIndexes = createUkrSignIndexes(fileUkrContentSpacesReplaced,sign2);

        Character sign3 = '.';
        ArrayList<Integer> dotIndexes = createSignIndexes(fileContentSpacesReplaced,sign3);
        ArrayList<Integer> dotUkrIndexes = createUkrSignIndexes(fileUkrContentSpacesReplaced,sign3);

        //System.out.println(questionIndexes.toString());
        //System.out.println(dotIndexes.toString());

        Integer maxQuestion = Collections.max(questionIndexes);
        Integer maxUkrQuestion = Collections.max(questionUkrIndexes);

        ArrayList<Integer> sentencesIndexes = new ArrayList<Integer>();
        ArrayList<Integer> sentencesUkrIndexes = new ArrayList<Integer>();
        sentencesIndexes.addAll(questionIndexes);
        sentencesIndexes.addAll(dotIndexes);
        sentencesUkrIndexes.addAll(questionUkrIndexes);
        sentencesUkrIndexes.addAll(dotUkrIndexes);
        Collections.sort(sentencesIndexes);
        Collections.sort(sentencesUkrIndexes);

        //System.out.println(sentencesIndexes);
        //System.out.println(sentencesUkrIndexes);

        sentencesIndexes.removeIf(senIndex -> senIndex > maxQuestion);
        sentencesUkrIndexes.removeIf(senUkrIndex -> senUkrIndex > maxUkrQuestion);

        //System.out.println(sentencesIndexes);
        //System.out.println(sentencesUkrIndexes);

        ArrayList<String> questionSentencesLexemes = createSelectedLexemes(sentencesIndexes,fileContentSpacesReplaced);
        ArrayList<String> questionUkrSentencesLexemes = createUkrSelectedLexemes(sentencesUkrIndexes,fileUkrContentSpacesReplaced);

        questionSentencesLexemes = concatLexemes(questionSentencesLexemes, "?");
        questionUkrSentencesLexemes = concatLexemes(questionUkrSentencesLexemes, "?");

        System.out.println(questionSentencesLexemes);
        System.out.println(questionUkrSentencesLexemes);

        Scanner sc= new Scanner(System.in);
        System.out.print("Enter a string: \n");
        String str= " "+sc.nextLine();              //reads string
        System.out.print("Enter string lenght: \n");
        Integer strLng= Integer.parseInt(sc.nextLine()) ;

        System.out.print("Введіть слово: \n");
        String strUkr= " " + sc.nextLine();              //читає рядок
        System.out.print("Введіть довжину: \n");
        Integer strLngUkr= Integer.parseInt(sc.nextLine()) ;

        System.out.println("You have entered: "+str+" with lenght "+strLng);
        System.out.println("Ви ввели: "+strUkr+" з довжиною "+strLngUkr);

        questionSentencesLexemes = concatLexemes(questionSentencesLexemes, str);
        questionUkrSentencesLexemes = concatLexemes(questionUkrSentencesLexemes, strUkr);

        System.out.println(questionSentencesLexemes);
        System.out.println(questionUkrSentencesLexemes);

        String questionedLexemesString = questionSentencesLexemes.toString();
        String[] wordsFromQuestionLexemes = questionedLexemesString.split(" ");

        for (int i =0; i < wordsFromQuestionLexemes.length; i++)
        {
            if (wordsFromQuestionLexemes[i].length() == strLng)
            {
                System.out.println(wordsFromQuestionLexemes[i]);
            }
        }

        String questionedUkrLexemesString = questionUkrSentencesLexemes.toString();
        String[] wordsFromUkrQuestionLexemes = questionedUkrLexemesString.split(" ");

        for (int i =0; i < wordsFromUkrQuestionLexemes.length; i++)
        {
            if (wordsFromUkrQuestionLexemes[i].length() == strLng)
            {
                System.out.println(wordsFromUkrQuestionLexemes[i]);
            }
        }
    }
}
