package com.example.laba3;

import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class Laba3Test {

    @org.junit.jupiter.api.Test
    void testSpacesReplace() {
        //Arrange
        String fileContent = "Juust   knlg   jk  hkg";
        Laba3 laba3 = new Laba3();
        String expected = "Juust knlg jk hkg";

        //Act
        String actual = Laba3.spacesReplace(fileContent);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @org.junit.jupiter.api.Test
    void testCreateSignIndexes() {
        //Arrange
        String fileContentSpacesReplaced = "klpgh";
        Character sign = 'p';
        Laba3 laba3 = new Laba3();
        int expected = 2;

        //Act
        int actual = laba3.createSignIndexes(fileContentSpacesReplaced,sign).get(0);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @org.junit.jupiter.api.Test
    void testCreateUkrSignIndexes() {
        //Arrange
        String fileUkrContentSpacesReplaced = "range";
        Character sign = 'g';
        Laba3 laba3 = new Laba3();
        int expected = 3;

        //Act
        int actual = laba3.createUkrSignIndexes(fileUkrContentSpacesReplaced,sign).get(0);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @org.junit.jupiter.api.Test
    void testCreateQuotationRating() {
        //Arrange
        String fileUkrContentSpacesReplaced = "rover";
        Character sign = 'r';
        Laba3 laba3 = new Laba3();
        int expected = 0;

        //Act
        int actual = laba3.createUkrSignIndexes(fileUkrContentSpacesReplaced,sign).get(0);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @org.junit.jupiter.api.Test
    void testAnotherCreateQuotationRating() {
        //Arrange
        String fileContentSpacesReplaced = "querying";
        Character sign = 'n';
        Laba3 laba3 = new Laba3();
        int expected = 6;

        //Act
        int actual = laba3.createSignIndexes(fileContentSpacesReplaced,sign).get(0);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
}
