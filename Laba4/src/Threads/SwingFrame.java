package Threads;

import java . awt . Dimension ;

import javax . swing . JFrame ; import javax . swing . JLabel ;

public class SwingFrame {
    public static void createGUI () {
        GaussianElimination gaus1 = new GaussianElimination();
        String reezz = gaus1.rez;
        //System.out.println(reezz);

        JFrame frame = new JFrame ( "Swing output window" );
        frame . setDefaultCloseOperation ( JFrame . EXIT_ON_CLOSE );

        JLabel label = new JLabel ( "Test label" );
        label.setText(reezz);

        frame . getContentPane (). add ( label );

        frame . setPreferredSize ( new Dimension ( 600 , 200 ));

        frame . pack ();
        frame . setVisible ( true );
    }

    public static void main ( String [] args ) {
        //Main mainProg = new Main();
        //mainProg

        JFrame . setDefaultLookAndFeelDecorated ( true );
        javax . swing . SwingUtilities . invokeLater ( new Runnable () {
            public void run () {
                createGUI ();
            }
        });
    }
}
